import javax.crypto.*;
import java.io.*;
import java.nio.channels.FileLock;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Random;

public class Main {
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static String usuario, pass, palabras = "";
    static File archivo= new File("fichero.txt");;

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, FileNotFoundException, IllegalBlockSizeException, BadPaddingException {
        SecretKey clave = null;
        generarUsuario();
        generarCadena();
        clave = generarClave();
        descifrarFichero("fichero.txt.cifrado", clave,
                "fichero2.txt.descifrado");
    }

    public static void generarUsuario() throws IOException {
        System.out.println("Introduce nombre de usuario:");
        usuario=reader.readLine();
        System.out.println("Introduce password:");
        pass=reader.readLine();
    }

    public static void generarCadena(){
        RandomAccessFile raf = null;
        int n; //para el número de palabras a generar.
        File archivo = null;
        FileLock bloqueo = null;
        String palabras = "";
        try{ //En este try ocurre toda la generación de palabras.
            archivo = new File("fichero.txt");
            raf = new RandomAccessFile(archivo,"rwd");
            bloqueo = raf.getChannel().lock(); //bloqueamos raf.
            StringBuffer buffer = null; //Creamos el buffer.
            String letras = new String("abcdefghijklmnñopqrstuvwxyz");
            int tamano = letras.length();
            Random random = new Random();
            for(int i = 1; i<5+1; i++)
            {
                int numero = (int)(Math.random() * 12 + 1); //Con esto hacemos que cada palabra
                //tenga un tamaño distinto al ponerlo en el segundo for.
                String palabra = "";
                for (int j = 1; j<=numero; j++)
                {
                    palabra += letras.charAt(random.nextInt(tamano));
                    palabras = palabra.replaceAll("\\s",""); //Quitamos tabulaciones y espacios en blanco.
                }
                System.out.println(palabras);
                raf.seek(raf.length()); //Colocamos raf al final del archivo.
                buffer = new StringBuffer(palabras); //Guardamos las palabras en el buffer.
                buffer.setLength(numero); //Igualamos el tamaño del buffer al tamaño de las palabras.
                raf.writeChars(buffer.toString().replaceAll("\\s",""));
                raf.writeChars("\n"); //Escribimos las palabras del buffer en raf, quitamos tabs y espacios blacos.
            }                       //Y le ponemos un salto de linea entre palabras.
            bloqueo.release(); //Liberamos el bloqueo del fichero
            bloqueo = null; //Dejamos el bloqueo en null.
        }catch(Exception e){
            System.err.println(e.toString());
        }finally{
            try{
                if( null != raf ) raf.close(); //Con este try se cierra raf y se libera el bloqueo
                if( null != bloqueo) bloqueo.release();
            }catch (Exception e2){
                System.err.println(e2.toString());
                System.exit(1);  //Si hay error se cierra.
            }
        }
    }

    public static SecretKey generarClave() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
            InvalidKeyException, FileNotFoundException, IOException, IllegalBlockSizeException, BadPaddingException {
        String key = usuario+pass;
        System.out.println(key);
        byte[] keyData = key.getBytes();
        System.out.println("Generando clave.");
        KeyGenerator generador = KeyGenerator.getInstance("AES");
        SecureRandom sec = new SecureRandom(key.getBytes());
        generador.init(128, sec);
        SecretKey clave = generador.generateKey();
        System.out.println("Clave: "+clave.getEncoded());
        FileInputStream fe = null;
        FileOutputStream fs = null;
        String formato="Rijndael/ECB/PKCS5Padding";
        Cipher cifrador = Cipher.getInstance("AES");
        cifrador.init(Cipher.ENCRYPT_MODE, clave);
        System.out.println("Se va a cifrar con AES el fichero: " + archivo
                + ", quedando como resultado " + archivo + ".cifrado");
        int bytesLeidos;
        byte[] buffer = new byte[1000]; //array de bytes
        byte[] bufferCifrado;
        fe = new FileInputStream(archivo); //objeto fichero de entrada
        fs = new FileOutputStream(archivo + ".cifrado"); //fichero de salida
        //lee el fichero de 1k en 1k y pasa los fragmentos leidos al cifrador
        bytesLeidos = fe.read(buffer, 0, 1000);
        while (bytesLeidos != -1) {//mientras no se llegue al final del fichero
            //pasa texto claro al cifrador y lo cifra, asignándolo a bufferCifrado
            bufferCifrado = cifrador.update(buffer, 0, bytesLeidos);
            fs.write(bufferCifrado); //Graba el texto cifrado en fichero
            bytesLeidos = fe.read(buffer, 0, 1000);
        }
        bufferCifrado = cifrador.doFinal(); //Completa el cifrado
        fs.write(bufferCifrado); //Graba el final del texto cifrado, si lo hay
        //Cierra ficheros
        fe.close();
        fs.close();
        System.out.println("Fichero cifrado generado con exito. " + archivo);
        return clave;
    }

    private static void descifrarFichero(String archivo1, SecretKey clave, String archivo2) throws NoSuchAlgorithmException,
            NoSuchPaddingException, FileNotFoundException, IOException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        FileInputStream fe = null; //fichero de entrada
        FileOutputStream fs = null; //fichero de salida
        int bytesLeidos;
        Cipher cifrador = Cipher.getInstance("AES");
        //3.- Poner cifrador en modo DESCIFRADO o DESENCRIPTACIÓN
        cifrador.init(Cipher.DECRYPT_MODE, clave);
        System.out.println("Se descrifara con AES el fichero: " + archivo1
                + ", y dejar en  " + archivo2 + "Recuerde dejar la extensión a .txt");
        fe = new FileInputStream(archivo1);
        fs = new FileOutputStream(archivo2);
        byte[] bufferClaro;
        byte[] buffer = new byte[1000]; //array de bytes
        //lee el fichero de 1k en 1k y pasa los fragmentos leidos al cifrador
        bytesLeidos = fe.read(buffer, 0, 1000);
        while (bytesLeidos != -1) {//mientras no se llegue al final del fichero
            //pasa texto cifrado al cifrador y lo descifra, asignándolo a bufferClaro
            bufferClaro = cifrador.update(buffer, 0, bytesLeidos);
            fs.write(bufferClaro); //Graba el texto claro en fichero
            bytesLeidos = fe.read(buffer, 0, 1000);
        }
        bufferClaro = cifrador.doFinal(); //Completa el descifrado
        fs.write(bufferClaro); //Graba el final del texto claro, si lo hay
        //cierra archivos
        fe.close();
        fs.close();
        System.out.println("Fichero "+ archivo2 +" descrifado con exito ");
    }
}